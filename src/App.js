import { Route, Routes } from 'react-router-dom';
import Home from './component/Home';
import FirstPage from './component/FirstPage';
import SecondPage from './component/SecondPage';
import ThirdPage from './component/ThirdPage'

function App() {
  return (
    <Routes>
      <Route path='/' Component={Home}/>
      <Route path='/first-page' Component={FirstPage}/>
      <Route path='/second-page/:param1/:param2' Component={SecondPage}/>
      <Route path='/third-page' Component={ThirdPage}/>
      <Route path='*' Component={Home}/>
    </Routes>
  );
}

export default App;

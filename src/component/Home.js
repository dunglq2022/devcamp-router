
import Header from '../component/Header'

function Home () {

    return (
        <div>
            <Header/>
            <h1 className="text-center">Welcome to the home page</h1>
        </div>
    )
}

export default Home
import Header from '../component/Header'

function FirstPage () {
    return (
        <div>
            <Header/>
            <h1 className="text-center">Welcome to the first page</h1>
        </div>
    )
}

export default FirstPage
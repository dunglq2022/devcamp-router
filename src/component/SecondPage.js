import { useEffect } from 'react';
import Header from '../component/Header'
import { useNavigate, useParams } from 'react-router-dom'

function SecondPage () {
    const {param1, param2} = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        if (param1 === 'third'){
            navigate('/third-page')
        }
        //eslint-disable-next-line
    }, [])

    return (
        <div>
            <Header/>
            <h1 className="text-center">Welcome to the second page param1 = {param1}, param2 = {param2}</h1>           
        </div>
    )
}

export default SecondPage